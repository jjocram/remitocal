//
//  SettingsView.swift
//  RemiToCal
//
//  Created by Marco Ferrati on 23/10/21.
//

import SwiftUI

@available(macOS 12.0, *)
@available(iOS 15.0, *)
struct SettingsView: View {
    @Binding var isPresented: Bool
    let eventManager: EventManager
    
    @State var calendar: String;
    
    
    @Binding var useDefaultCalendar: Bool
    @Binding var createRemiToCalCalendar: Bool
    @Binding var defaultCalendarIdentifier: String
    @Binding var minutes: Int
    @Binding var allDayEvent: Bool
    @Binding var newEventTime: String
    
    var body: some View {
        NavigationView {
            Form {
                Section(header: Text("Where the new eventes will be saved")) {
                    Toggle("Use default calendar", isOn: $useDefaultCalendar)
                    if !self.useDefaultCalendar {
                        Toggle("Use the created RemiToCal calendar", isOn: $createRemiToCalCalendar)
                        if !createRemiToCalCalendar {
                            Picker("Default calendar", selection: eventManager.$defaultCalendarIdentifier) {
                                ForEach(eventManager.getCalendarsForEvents(), id: \.self.calendarIdentifier) {cal in Text(cal.title)}
                            }
                        }
                    }
                }
                
                Section(header: Text("Created events")) {
                    VStack{
                        TextField("Minutes", value: $minutes, format: .number)
                        Text("How long does the created event last (in minutes)")
                            .font(.footnote)
                            .foregroundColor(.gray)
                    }
                    
                    
                    
                    //                    Text("Create all day events for reminder without a specified time")
                    //                        .font(.footnote)
                    VStack{
                        Toggle("All day events", isOn: $allDayEvent)
                        if !allDayEvent {
                            DatePicker("Event time", selection: Binding<Date>(get: {DateFormatter.timeFormatter.date(from: newEventTime) ?? Date()},
                                                                              set: {newEventTime = DateFormatter.timeFormatter.string(from: $0)}),
                                       displayedComponents: [.hourAndMinute])
                        }
                    }
                    
                }
            }
            .navigationTitle("Settings")
            .toolbar {
                ToolbarItem {
                    Button(action: {isPresented = false}, label: {Text("Done")})
                }
            }
        }
    }
}

//struct SettingsView_Previews: PreviewProvider {
//    static var previews: some View {
//        SettingsView(isPresented: $true)
//    }
//}
