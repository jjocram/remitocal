//
//  RemiToCalApp.swift
//  Shared
//
//  Created by Marco Ferrati on 20/10/21.
//

import SwiftUI

final class AppDelegate: NSObject, UIApplicationDelegate {
    
    var shortcutItem: UIApplicationShortcutItem? { AppDelegate.shortcutItem }
    
    fileprivate static var shortcutItem: UIApplicationShortcutItem?
    
    func application(
        _ application: UIApplication,
        configurationForConnecting connectingSceneSession: UISceneSession,
        options: UIScene.ConnectionOptions
    ) -> UISceneConfiguration {
        if let shortcutItem = options.shortcutItem {
            AppDelegate.shortcutItem = shortcutItem
        }
        
        let sceneConfiguration = UISceneConfiguration(
            name: "Scene Configuration",
            sessionRole: connectingSceneSession.role
        )
        sceneConfiguration.delegateClass = SceneDelegate.self
        
        return sceneConfiguration
    }
}

private final class SceneDelegate: UIResponder, UIWindowSceneDelegate {
    func windowScene(
        _ windowScene: UIWindowScene,
        performActionFor shortcutItem: UIApplicationShortcutItem,
        completionHandler: @escaping (Bool) -> Void
    ) {
        AppDelegate.shortcutItem = shortcutItem
        completionHandler(true)
    }
}

@available(macOS 12.0, *)
@available(iOS 15.0, *)
@main
struct RemiToCalApp: App {
    @UIApplicationDelegateAdaptor(AppDelegate.self) var appDelegate
    @Environment(\.scenePhase) var scenePhase
    let persistenceController = PersistenceController.shared
    let eventManagerShared = EventManager.shared
    
    @State var permissionOk: Bool = EventManager.shared.hasAccessTo(.reminder) && EventManager.shared.hasAccessTo(.event)
    
    @State var launchFromShortcut: Bool = false

    var body: some Scene {
        WindowGroup {
            if permissionOk{
                ContentView(launchFromShortcut: $launchFromShortcut)
                    .environment(\.managedObjectContext, persistenceController.container.viewContext)
                    .environmentObject(eventManagerShared)
                    .frame(minWidth: 50, idealWidth: 50, maxWidth: .infinity, minHeight: 50, idealHeight: 50, maxHeight: .infinity)
            }
            else {
                SetupView(showReminderRequest:Binding<Bool>(get: {!eventManagerShared.hasAccessTo(.reminder)},
                                                            set: {_ in }),
                          showCalendarRequest: Binding<Bool>(get: {!eventManagerShared.hasAccessTo(.event)},
                                                             set: {_ in }),
                            permissionOk: $permissionOk)
                    .environment(\.managedObjectContext, persistenceController.container.viewContext)
                    .environmentObject(eventManagerShared)
                    .frame(minWidth: 50, idealWidth: 50, maxWidth: .infinity, minHeight: 50, idealHeight: 50, maxHeight: .infinity)
            }
        }
        .onChange(of: scenePhase) { phase in
            switch scenePhase {
            case .inactive: //.incative do what .active should do. Why? IDk
                guard let shortcutItem = appDelegate.shortcutItem else { return }
                if shortcutItem.type == "syncAndImport"{
                        launchFromShortcut = true
                }
            default:
//                print("Not handled scene phase: \(scenePhase)")
                return
            }
        }
    }
}
