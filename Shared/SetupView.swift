//
//  SetupView.swift
//  RemiToCal
//
//  Created by Marco Ferrati on 25/10/21.
//

import SwiftUI
import Contacts
import EventKit

@available(iOS 15.0, *)
struct SetupView: View {
    @Environment(\.managedObjectContext) private var viewContext
    @EnvironmentObject private var eventManager: EventManager
    
    @Binding var showReminderRequest: Bool
    @Binding var showCalendarRequest: Bool
    
    @Binding var permissionOk: Bool
    
    struct RequestPermission: View {
        @EnvironmentObject private var eventManager: EventManager
        
        let icon: String
        let title: String
        let hint: String
        let buttonLabel: String
        let resource: EKEntityType
        let completionHandler: () -> Void
        let bottomHeight: CGFloat
        
        var body: some View {
            HStack {
                Image(systemName: icon)
                Text(title)
                    .font(.title)
            }
            
            Text(hint)
            
            Button(buttonLabel){
                eventManager.requestAccessTo(resource,
                                             completionHandler: completionHandler,
                                             errorHandler: { openSettingsApp() })
            }
            
            Spacer()
                .frame(width: 0, height: bottomHeight, alignment: .center)
        }
        
        private func openSettingsApp() {
            DispatchQueue.main.sync {
                UIApplication.shared.open(URL(string: UIApplication.openSettingsURLString)!)
            }
        }
    }
    
    var body: some View {
        Text("Setup Reminder To Calendar")
            .font(.title)
        
        VStack {
            if showReminderRequest {
                RequestPermission(icon: "smallcircle.filled.circle",
                                  title: "Permission for reminders",
                                  hint: "Tap on the button below to give access to your reminders",
                                  buttonLabel: "Give access to the reminder",
                                  resource: .reminder,
                                  completionHandler: {
                    showReminderRequest = false
                    permissionOk = showCalendarRequest ? false : true
                },
                                  bottomHeight: 50.0)
            }
            
            if showCalendarRequest {
                RequestPermission(icon: "calendar.circle",
                                  title: "Permission for calendar",
                                  hint: "Tap on the button below to give access to your reminders",
                                  buttonLabel: "Give access to the calendar",
                                  resource: .event,
                                  completionHandler: {
                    showCalendarRequest = false
                    permissionOk = showReminderRequest ? false : true
                },
                                  bottomHeight: 0.0)
            }
        }
    }
}

//struct SetupView_Previews: PreviewProvider {
//    static var previews: some View {
//        SetupView()
//    }
//}
