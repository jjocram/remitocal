//
//  EventManager.swift
//  RemiToCal
//
//  Created by Marco Ferrati on 22/10/21.
//

import Foundation
import EventKit
import SwiftUI

extension DateFormatter {
    static let timeFormatter: DateFormatter = {
        let df = DateFormatter()
        df.dateStyle = .none
        df.timeStyle = .short
        
        return df
    }()
    
    static let dateTimeFormatter: DateFormatter = {
        let df = DateFormatter()
        df.dateStyle = .short
        df.timeStyle = .short
        return df
    }()
}

class EventManager: ObservableObject {
    static let shared = EventManager()
    
    private let store: EKEventStore
    private let completedRemindersPredicate: NSPredicate
    private let remindersIncompletePredicate: NSPredicate
    
    @AppStorage("useDefaultCalendar", store: .standard) var useDefaultCalendar: Bool = true
    @AppStorage("createRemiToCalCalendar", store: .standard) var createRemiToCalCalendar: Bool = true
    @AppStorage("defaultCalendarIdentifier", store: .standard) var defaultCalendarIdentifier: String = "notSet"
    @AppStorage("minutesForCreatedEvents", store: .standard) var minutes: Int = 5
    @AppStorage("allDayEvent", store: .standard) var allDayEvent: Bool = true
    @AppStorage("newEventTime", store: .standard) var newEventTime: String = DateFormatter.timeFormatter.string(from: Date())
    
    init() {
        self.store = EKEventStore()
        self.completedRemindersPredicate = store.predicateForCompletedReminders(withCompletionDateStarting: nil, ending: nil, calendars: nil)
        self.remindersIncompletePredicate = store.predicateForIncompleteReminders(withDueDateStarting: nil, ending: nil, calendars: nil)
    }
    
    func requestAccessTo(_ resource: EKEntityType, completionHandler: @escaping () -> Void, errorHandler: @escaping () -> Void) {
        store.requestAccess(to: resource) { granted, error in
            if granted {
                completionHandler()
            } else {
                print("\(resource)-\(resource.rawValue) persmission not granted. Problem or user error: \(String(describing: error))")
                errorHandler()
            }
        }
    }
    
    func hasAccessTo(_ resource: EKEntityType) -> Bool{
        return EKEventStore.authorizationStatus(for: resource) == .authorized
    }
    
    func deleteEvent(item: Item) {
        do {
            if let event = store.event(withIdentifier: item.calendarReferenceIdentifier!) {
                try store.remove(event, span: item.recurrent ? .futureEvents : .thisEvent)
            }
            else {
                print("Cannot locate this event \(item.title!) in calendar")
            }
            
            item.calendarReferenceIdentifier = nil
            item.imported = false
        } catch {
            print("Cannot delete event")
        }
    }
    
    private func setDateTimeFor(_ event: EKEvent, from reminder: Item){
        if reminder.missingTime {
            // The time is not defined
            if allDayEvent{
                // The allDay events is selected
                event.startDate = reminder.dueDate!
                event.endDate = reminder.dueDate!.addingTimeInterval(TimeInterval(minutes*60)) //Necessary for some unknown reason
                event.isAllDay = true
            }
            else {
                // A specific time is setted
                let dateFromSettings = Calendar.current.dateComponents([.hour, .minute], from: DateFormatter.timeFormatter.date(from: newEventTime)!)
                
                var startDate = Calendar.current.dateComponents([.year, .month, .day, .calendar, .timeZone], from: reminder.dueDate!)
                startDate.hour = dateFromSettings.hour
                startDate.minute = dateFromSettings.minute
                
                event.startDate = startDate.date!
                event.endDate = startDate.date!.addingTimeInterval(TimeInterval(minutes*60))
            }
        }
        else {
            // The time is known
            event.startDate = reminder.dueDate!
            event.endDate = reminder.dueDate!.addingTimeInterval(TimeInterval(minutes*60))
        }
    }
    
    func importReminder(_ reminder: Item, commit: Bool = true){
        let newEvent = EKEvent(eventStore: store)
        newEvent.title = reminder.title!
        
        setDateTimeFor(newEvent, from: reminder)
        
        newEvent.calendar = getUserCalendar()!
        
        if reminder.recurrent {
            for recurrence in getRecurrenciesRule(from: reminder.identifier!) {
                newEvent.addRecurrenceRule(EKRecurrenceRule(recurrenceWith: recurrence.frequency,
                                                            interval: recurrence.interval,
                                                            end: recurrence.recurrenceEnd))
            }
        }
        //newEvent.addAlarm(EKAlarm(absoluteDate: newEvent.startDate!)) // Maybe no (the reminder do the allarm thing)
        reminder.calendarReferenceIdentifier = newEvent.calendarItemIdentifier
        do {
            try store.save(newEvent, span: reminder.recurrent ? .futureEvents : .thisEvent, commit: commit)
            reminder.imported = true
        } catch {
            print("Cannot save the new event")
        }
    }
    
    func importReminders(reminders: [Item]) {
        reminders.forEach({importReminder($0, commit: false)})
        
        do {
            try store.commit()
        } catch{
            print("Cannot commit changes to calendar")
        }
    }
    
    func fetchCompletedReminders(closure: @escaping ([EKReminder]) -> Void) {
        store.fetchReminders(matching: completedRemindersPredicate) { reminders in
            if let reminders = reminders {
                closure(reminders)
            }
        }
    }
    
    func fetchIncompletedReminders(closure: @escaping ([EKReminder]) -> Void) {
        store.fetchReminders(matching: remindersIncompletePredicate) { reminders in
            if let reminders = reminders {
                closure(reminders)
            }
        }
    }
    
    func getCalendarItem(withIdentifier identifier: String) -> EKCalendarItem?{
        store.calendarItem(withIdentifier: identifier)
    }
    
    private func save(event: EKEvent, repeated: Bool = false) {
        do {
            try store.save(event, span: repeated ? .futureEvents : .thisEvent)
        } catch {
            print("Cannot save item in calendar")
        }
    }
    
    func updateEvent(withItem item: Item) {
        if let calendarEvent = store.event(withIdentifier: item.calendarReferenceIdentifier!){
            calendarEvent.title = item.title
            
            // Setup date
            setDateTimeFor(calendarEvent, from: item)
            
            if item.recurrent {
                calendarEvent.recurrenceRules?.forEach({calendarEvent.removeRecurrenceRule($0)})
                
                for recurrence in getRecurrenciesRule(from: item.identifier!) {
                    calendarEvent.addRecurrenceRule(EKRecurrenceRule(recurrenceWith: recurrence.frequency,
                                                                     interval: recurrence.interval,
                                                                     end: recurrence.recurrenceEnd))
                }
            }
            save(event: calendarEvent, repeated: item.recurrent)
        } else {
            print("Cannot locate event to edit")
        }
    }
    
    private func getRecurrenciesRule(from identifier: String) -> [EKRecurrenceRule]{
        if let reminder = getCalendarItem(withIdentifier: identifier) as? EKReminder {
            if reminder.hasRecurrenceRules {
                return reminder.recurrenceRules!
            }
            else {
                return []
            }
        } else {
            print("Cannot locate reminder to get the recurrencies")
            return []
        }
    }
    
    func getCalendarsForEvents() -> [EKCalendar] {
        return store.calendars(for: .event)
    }
    
    private func getCalendar(withIdentifier identifier: String) -> EKCalendar? {
        return store.calendar(withIdentifier: identifier)
    }
    
    func getDefaultCalendar() -> EKCalendar? {
        getCalendarsForEvents().first(where: {$0 == store.defaultCalendarForNewEvents})
    }
    
    private func getUserDefaultCalendar() -> EKCalendar? {
        if defaultCalendarIdentifier == "notSet" {
            return store.defaultCalendarForNewEvents
        }
        
        return getCalendar(withIdentifier: defaultCalendarIdentifier)
    }
    
    private func getRemiToCalCalendar() -> EKCalendar? {
        if let remiToCalCalendar = getCalendar(withIdentifier: UserDefaults.standard.string(forKey: "remiToCalCalendarIndentifier") ?? "ObviuslyNotAnEsistantIdentifier") {
            return remiToCalCalendar
        }
        else {
            let remiToCalCalendar = EKCalendar(for: .event, eventStore: store)
            remiToCalCalendar.title = "RemiToCal"
            remiToCalCalendar.source = store.defaultCalendarForNewEvents?.source
            do {
                try store.saveCalendar(remiToCalCalendar, commit: true)
                UserDefaults.standard.set(remiToCalCalendar.calendarIdentifier, forKey: "remiToCalCalendarIndentifier")
                return remiToCalCalendar
            } catch {
                print("Cannot create RemiToCal calendar.")
                return nil
            }
        }
    }
    
    private func getUserCalendar() -> EKCalendar? {
        if useDefaultCalendar {
            return store.defaultCalendarForNewEvents //TODO: could be absent, if so, create the RemiToCal calendar
        }
        
        if createRemiToCalCalendar {
            return getRemiToCalCalendar()
        }
        
        // Default behaviour: return the default calendar used by the user or the one specified as defult
        return getUserDefaultCalendar()
    }
    
}
