//
//  SettingsManager.swift
//  RemiToCal
//
//  Created by Marco Ferrati on 23/10/21.
//

import Foundation
import SwiftUI

class SettingsManager: ObservableObject {
    static let shared = SettingsManager()
}
