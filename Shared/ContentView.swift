//
//  ContentView.swift
//  Shared
//
//  Created by Marco Ferrati on 20/10/21.
//

import SwiftUI
import CoreData
import EventKit

@available(macOS 12.0, *)
@available(iOS 15.0, *)
struct ContentView: View {
    
    @Environment(\.managedObjectContext) private var viewContext
    @EnvironmentObject private var eventManager: EventManager
    
    @State var showSettingsModalView: Bool = false
    
    @AppStorage("calendar", store: .standard) var calendar: String = ""
    
    @FetchRequest(
        sortDescriptors: [NSSortDescriptor(keyPath: \Item.title, ascending: true)],
        animation: .default)
    private var items: FetchedResults<Item>
    
    @Binding var launchFromShortcut: Bool;
    
    var body: some View {
        VStack {
            List {
                Section(header: Text("Elements to be imported")) {
                    ForEach(items.filter({$0.imported == false && $0.completed == false})) { item in
                        EventItem(event: item)
                            .frame(maxWidth: .infinity)
                            .padding()
                            .onTapGesture {
                                eventManager.importReminder(item)
                            }
                    }
                    
                }
                Section(header: Text("Elements already imported")) {
                    ForEach(items.filter({$0.imported == true && $0.completed == false})) { item in
                        EventItem(event: item)
                            .padding()
                            .onTapGesture {
                                deImportItem(item)
                            }
                    }
                    .onDelete(perform: deImportItems)
                }
            }
            .refreshable {
                refreshItems()
            }
            
            Spacer()
            
            HStack {
                Spacer()
                
                Button(action: {
                    showSettingsModalView = true
                }, label: {
                    VStack{
                        Image(systemName: "gear")
                            .font(.system(size: 20))
                        Text("Settings")
                    }
                })
                .sheet(isPresented: $showSettingsModalView, onDismiss: nil, content: {
                    SettingsView(isPresented: $showSettingsModalView,
                                 eventManager: eventManager,
                                 calendar: eventManager.getDefaultCalendar()?.title ?? "Select a calendar",
                                 useDefaultCalendar: eventManager.$useDefaultCalendar,
                                 createRemiToCalCalendar: eventManager.$createRemiToCalCalendar,
                                 defaultCalendarIdentifier: eventManager.$defaultCalendarIdentifier,
                                 minutes: eventManager.$minutes,
                                 allDayEvent: eventManager.$allDayEvent,
                                 newEventTime: eventManager.$newEventTime)
                })
                .keyboardShortcut("s")
                
                Spacer()
                
                Button(action: refreshItems) {
                    VStack{
                        Image(systemName: "gobackward")
                            .font(.system(size: 20))
                        Text("Refresh")
                    }
                }
                .keyboardShortcut("r")
                
                Spacer()
                
                Button(action: importAllReminders) {
                    VStack{
                        Image(systemName: "square.and.arrow.down.on.square")
                            .font(.system(size: 20))
                        Text("Import all")
                    }
                }
                .keyboardShortcut("i")
                
                Spacer()
            }
        }
        .onChange(of: launchFromShortcut) { fromShortcut in
            if(fromShortcut){
                refreshItems()
                importAllReminders()
                launchFromShortcut = false
            }
        }
        .onAppear {
            refreshItems()
        }
        
    }
    
    private func importAllReminders() {
        eventManager.importReminders(reminders: items.filter({$0.imported == false && $0.completed == false}))
    }
    
    private func refreshItems() {
        eventManager.fetchIncompletedReminders { reminders in
            for reminder in reminders {
                if shouldCreateItem(reminder: reminder) {
                    createItemWithContext(reminder: reminder)
                }
            }
            removeDeleted()
            removeCompleted()
            saveContext()
        }
    }
    
    private func createItemWithContext(reminder: EKReminder) {
        let newItem = Item(context: viewContext)
        newItem.title = reminder.title
        newItem.identifier = reminder.calendarItemIdentifier
        newItem.dueDate = reminder.dueDateComponents!.date
        newItem.recurrent = reminder.hasRecurrenceRules
        newItem.imported = false
        newItem.completed = false
        newItem.creationDate = reminder.creationDate
        newItem.missingTime = reminder.dueDateComponents!.hour == nil
    }
    
    private func saveContext() {
        do {
            try viewContext.save()
        } catch {
            // Replace this implementation with code to handle the error appropriately.
            // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
            let nsError = error as NSError
            fatalError("Unresolved error \(nsError), \(nsError.userInfo)")
        }
    }
    
    private func removeCompleted() {
        eventManager.fetchCompletedReminders { reminders in
            for reminder in reminders{
                if let itemToUpdate = items.filter({$0.completed == false}).first(where: {$0.identifier == reminder.calendarItemIdentifier}) {
                    if itemToUpdate.imported {
                        eventManager.deleteEvent(item: itemToUpdate) //TODO: Settings remove completed from calendar?
                    }
                    itemToUpdate.completed = true
                }
            }
        }
    }
    
    private func removeDeleted() {
        items.filter({eventManager.getCalendarItem(withIdentifier: $0.identifier!) == nil}).forEach { item in
            if let _ = item.calendarReferenceIdentifier {
                eventManager.deleteEvent(item: item)
            }
            viewContext.delete(item)
        }
        saveContext()
    }
    
    private func shouldCreateItem(reminder: EKReminder) -> Bool {
        // If the item has not a due date then should not be created
        if reminder.dueDateComponents == nil {
            return false
        }
        // If is possible to find and element with the same identifier then item should not be created
        // This is useful to update elements already existants
        if let item = items.first(where: {$0.identifier == reminder.calendarItemIdentifier}) {
            updateItemAndEvent(item: item, reminder: reminder)
            return false
        }
        
        // Otherwise return true
        return true
    }
    
    private func updateItemAndEvent(item: Item, reminder: EKReminder) {
        if item.completed != reminder.isCompleted{
            //Update already existant item
            item.completed = reminder.isCompleted
        }
        
        if let reminderEditDate = reminder.lastModifiedDate {
            if item.creationDate != reminderEditDate {
                //Item was changed. Update it
                item.title = reminder.title
                item.dueDate = reminder.dueDateComponents?.date
                item.recurrent = reminder.hasRecurrenceRules
                item.creationDate = reminderEditDate
                item.missingTime = reminder.dueDateComponents!.hour == nil
                if let _ = item.calendarReferenceIdentifier {
                    eventManager.updateEvent(withItem: item)
                }
            }
        }
    }
    
    private func deImportItems(offsets: IndexSet) {
        withAnimation {
            offsets.map { items.filter({$0.imported == true && $0.completed == false})[$0] }.forEach { item in
                eventManager.deleteEvent(item: item)
            }
            
            saveContext()
        }
    }
    
    private func deImportItem(_ item: Item) {
        withAnimation {
            eventManager.deleteEvent(item: item)
            saveContext()
        }
    }
}



//struct ContentView_Previews: PreviewProvider {
//    static var previews: some View {
//        ContentView().environment(\.managedObjectContext, PersistenceController.preview.container.viewContext)
//    }
//}
