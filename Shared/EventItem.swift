//
//  EventItem.swift
//  RemiToCal
//
//  Created by Marco Ferrati on 21/08/22.
//

import SwiftUI

struct EventItem: View {
    let event: Item
    var body: some View {
        HStack{
            
            Text("\(event.title ?? "Missing title")\n") + 
            Text(DateFormatter.dateTimeFormatter.string(from: (event.dueDate)!))
                .font(.footnote)
            
            Spacer() // Whole row cliccable pt 1/2
        }
        .contentShape(Rectangle()) // Whole row cliccalble pt 2/2
    }
}

//struct EventItem_Previews: PreviewProvider {
//    static var previews: some View {
//        EventItem()
//    }
//}
